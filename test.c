#include <rpi-input.h>

#include <stdlib.h>
#include <stdio.h>

void checkinput (void) {
  InputEvent event;

  while (InputPollEvent(&event)) {
    switch (event.type) {
      case INPUT_MOUSEMOTION:
        printf("mouse moved by %d,%d\n", event.motion[0], event.motion[1]);
        break;
      
      case INPUT_MOUSEBUTTONDOWN:
        printf("mouse button %d down\n", event.code);
        break;
      
      case INPUT_KEYDOWN:
        if (event.code == 16) { // Q
            InputClose();
            exit(0);
        }
      
        printf("key code %d down\n", event.code);
        break;
    }
  }
}

int main (void) {

  InputInit();

  while (1) {
    
    /* Do stuff here. */
  
    checkinput();
    usleep(50*1000);
  }
}
